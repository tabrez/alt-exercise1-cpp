#include<iostream>
#include<chrono>
#include"helpers.hpp"
#include"order_book.hpp"
using namespace std;

void output_on_exit(const OrderBook& book) {
  cout << endl << endl << "-----Exiting program-----";
  string str;
  book.print_products(str);
  cout << str;
  str = "";
  book.print_errors(str);
  cout << str;
}

void example1() {
  OrderBook book;
  if(book.process_file("messages.txt")) {
    output_on_exit(book);
  }
}

void example2() {
    vector<string> messages = {
      "N,5,100001,B,3,1500",
      "N,5,100002,B,4,175",
      "N,5,100003,S,2,175",
      "N,4,100004,B,11,200",
      "N,5,100005,B,7,175",
      "N,4,100006,S,8,175",
      "N,5,100007,S,7,2500",
      "N,5,100007,B,3,1200",   // duplicate
      "N,4,100007,S,12,200",  // duplicate
      "M,100007,S,16,200",
      "R,100001,B,3,1500",
      "R,100006,S,8,175",
      "M,100002,B,3,175",
      "N,4,100008,B,3,1500",
      "N,5,100001,B,13,500",
      "N,5,100001,B,3,1500",  // duplicate
      "R,100001,B,3,1500",    // doesn't exist
      "M,100001,B,17,500",
      "R,100001,B,17,500",
      "M,100001,B,17,500",    // doesn't exist
      "M,100004,B,5,200",
      "N,4,100001,S,17,500",
      "N,5,100006,B,7,2500",
      "N,5,100009,S,19,2500",
      "R,100005,B,7,175",
      "M,100001,S,8,500",
      "M,100005,B,5,175",       // doesn't exist
      "M,100004,B,16,200",
      "N,4,100011,S,2,1400",
      "X,4,5,200",
      "X,4,7,200",
      "X,4,2,200",
      "X,5,14,1300",
      "X,4,6,300",
      "X,5,3,1300",
      "X,4,1,100",
      "M,100006,S,16,2500",       // wrong 'side'
      "M,100006,B,16,2500",
      "R,100006,S,16,2500"        // wrong 'side'
    };

  OrderBook book;
  book.process_vector(messages);
  output_on_exit(book);
}

// performance tests
void example3() {
    const vector<string> insert_messages = {
    "N,5,100001,B,3,1500",
    "N,5,100002,B,1,175",
    "N,5,100003,B,1,175",
    "N,4,100004,B,1,200",
    "N,5,100005,B,1,175",
    "N,4,100006,S,1,175",
    "N,5,100007,S,1,200",
  };

  const vector<string> modify_messages = {
    "M,100001,B,1,1500",
    "M,100002,B,11,175",
    "M,100003,B,22,175",
    "M,100004,B,31,200",
    "M,100005,B,4,175",
    "M,100006,S,9,175",
    "M,100007,S,29,200",
  };

  const vector<string> remove_messages = {
    "R,100001,B,1,1500",
    "R,100002,B,11,175",
    "R,100003,B,22,175",
    "R,100004,B,31,200",
    "R,100005,B,4,175",
    "R,100006,S,9,175",
    "R,100007,S,29,200",
  };
  const vector<string> trade_messages = {
    "X,4,1,1500",
    "X,5,11,175",
    "X,5,22,175",
    "X,5,131,200",
    "X,5,4,175",
    "X,4,9,175",
    "X,4,29,200",
  };

  OrderBook book;

  auto start = chrono::high_resolution_clock::now();
  for(auto i = 0; i < 500; ++i) {
    book.process_vector(insert_messages);
    book.process_vector(modify_messages);
    book.process_vector(remove_messages);
    book.process_vector(trade_messages);
  }
  auto end = chrono::high_resolution_clock::now();
  cout << "Total time: ";
  cout << chrono::duration<double, std::milli>(end - start).count() / 1000 << 's';
}

void example4() {
  OrderBook book;
  auto start = chrono::high_resolution_clock::now();
  int i = 0;
  for(; i < 5000; ++i) {
    string msg = "N,5,10000";
    msg += to_string(i);
    msg += ",B,5,100";
    cout <<  endl << "Processing: " + msg;
    book.process_message(msg);
  }
  for(; i < 10000; ++i) {
    string msg = "N,5,10000";
    msg += to_string(i);
    msg += ",S,5,100";
    Order order;
    to_order(msg, order);
    cout <<  endl << "Processing: " + msg;
    book.process_message(msg);
    book.is_trade_message_expected(order);
  }
  cout << endl << book.count();
  auto end = chrono::high_resolution_clock::now();
  cout << endl << "Total time: ";
  cout << chrono::duration<double, std::milli>(end - start).count() / 1000 << 's';
}

int main() {
  example2();
}
