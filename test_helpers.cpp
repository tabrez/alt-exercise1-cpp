#define BOOST_TEST_MODULE "Test Helpers"
#include <boost/test/included/unit_test.hpp>
#include <algorithm>
#include "helpers.hpp"
using namespace std;

bool validate_message(const string& message);
void parse(const string& msg, vector<string>& tokens);
bool tokens_to_order(const vector<string>& tokens, Order& message);

template <typename T>
void is_true(T f) {
  BOOST_TEST(f());
}

template <typename T>
void is_false(T f) {
  BOOST_TEST(!f());
}

BOOST_AUTO_TEST_SUITE(Test_Helpers)

BOOST_AUTO_TEST_CASE(test_validate_message_correct_messages) {
  auto messages = { 
    "N,5,14211,B,3,1500",
    "N,5,100001,B,1,175",
    "R,100001,B,1,175",
    "M,100001,B,1,175",
    "N,5,100001,S,1,175",
    "R,100001,S,1,175",
    "M,100001,S,1,175",
    "X,5,1,105"
  };
  // for_each(messages.begin(), messages.end(), [](string msg) { 
  //     BOOST_TEST(validate_message(msg));
  // });
  for(const string msg: messages)
    is_true(bind(validate_message, msg));
}

BOOST_AUTO_TEST_CASE(test_validate_message_incorrect_messages) {
  auto messages = {
    "hello world",
    "12345",
    "N,5,100001,T,1,175",
    "R,5,100001,B,1,175",
    "M,5,100001,B,1,175",
    "X,5,100001,B,1,175",
    "R,B,1,175",
    "M,100001,1,175",
    "N,5,100001,S,175",
    "R,100001,S,1",
    "M,100001,S,hello,175",
    "M,100001,Y,hello,175",
    "X,1,1050",
    "N,1,1050",
    "R,1,1050",
    "M,1,1050",
    "Y,1,2,1050",
    "Y,5,100001,B,1,175",
    "Y,100001,B,1,175"
  };
  for(const string msg: messages)
    is_false(bind(validate_message, msg));
}

BOOST_AUTO_TEST_CASE(test_validate_message_negative_values) {
  auto messages = {
    "N,-5,100001,B,1,175",
    "R,-100001,S,1,175",
    "M,100001,B,-1,175",
    "N,5,100001,S,1,-175",
    "N,-5,-100001,B,-1,-175",
  };
  for(const string msg: messages)
    is_false(bind(validate_message, msg));
}

BOOST_AUTO_TEST_CASE(test_validate_message_missing_values) {
  auto messages = {
    "N,100001,B,1,175",
    "N,100001,S,1,175",
    "N,10,1,175",
    "N,10,1,175",
    "N,100001,B,175",
    "N,100001,S,175",
    "N,100001,S",
    "N,S,175",
    "R,B,1,175",
    "R,100001,1,175",
    "R,100001,B,1",
    "R,100001,B,",
    "R,S,",
    "M,B,1,175",
    "M,100001,1,175",
    "M,100001,S,1",
    "M,100001,S,",
    "M,S,1",
  };
  for(const string msg: messages)
    is_false(bind(validate_message, msg));
}

BOOST_AUTO_TEST_CASE(test_validate_message_out_of_bound_values) {
  auto messages = { 
    "N,5,14211,B,3,1234567",
    "N,5,100001,B,1234,175",
    "R,10000100011,B,1,175",
    "M,10000100011,B,1,175",
    "N,123456,100001,S,1,175",
    "R,100001,S,1234,175",
    "M,100001,S,1,1234567",
    "X,654321,1,105"
  };
  for(const string msg: messages)
    is_false(bind(validate_message, msg));
}

BOOST_AUTO_TEST_CASE(test_parse_new_messages) {
  auto messages = { 
    "N,5,14211,B,3,1500",
    "N,5,100001,B,1,175",
    "N,5,100001,S,1,175",
  };
  vector<string> tokens;
  for(const string msg: messages) {
    parse(msg, tokens);
    BOOST_TEST(tokens.size() == 6);
    BOOST_TEST(tokens[0] == "N");
    BOOST_TEST(tokens[1] == "5");
  }
}

BOOST_AUTO_TEST_CASE(test_parse_remove_modify_messages) {
  auto messages = { 
    "R,100001,B,1,175",
    "M,100001,B,1,175",
    "R,100001,S,1,175",
    "M,100001,S,1,175",
  };
  vector<string> tokens;
  for(const string msg: messages) {
    parse(msg, tokens);
    BOOST_TEST(tokens.size() == 5);
    BOOST_TEST((tokens[0] == "M" || tokens[0] == "R"));
  }
}

BOOST_AUTO_TEST_CASE(test_parse_trade_messages) {
  auto messages = { 
    "X,5,1,105",
    "X,153,21,2500"
  };
  vector<string> tokens;
  for(const string msg: messages) {
    parse(msg, tokens);
    BOOST_TEST(tokens.size() == 4);
    BOOST_TEST(tokens[0] == "X");
  }
}

BOOST_AUTO_TEST_CASE(test_tokens_to_order_for_new_messages) {
  auto msg = "N,5,14211,B,3,1500";
  vector<string> tokens;
  parse(msg, tokens);
  Order order;
  auto res = tokens_to_order(tokens, order);
  BOOST_TEST(res);
  BOOST_TEST(order.action == Action::N);
  BOOST_TEST(order.product_id == "5");
  BOOST_TEST(order.order_id == "14211");
  BOOST_TEST(order.side == Side::B);
  BOOST_TEST(order.quantity == 3);
  BOOST_TEST(order.price == 1500);

  auto messages = { 
    "N,5,14211,B,3,1500",
    "N,5,100001,B,1,75",
    "N,5,100001,S,17,1000",
  };
  for(const string msg: messages) {
    vector<string> tokens;
    parse(msg, tokens);
    Order order;
    auto res = tokens_to_order(tokens, order);
    BOOST_TEST(res);
    BOOST_TEST(order.action == Action::N);
    BOOST_TEST(order.product_id == "5");
  }
}

BOOST_AUTO_TEST_CASE(test_tokens_to_order_for_remove_modify_messages) {
  auto msg = "R,100001,B,1,175";
  vector<string> tokens;
  parse(msg, tokens);
  Order order;
  auto res = tokens_to_order(tokens, order);
  BOOST_TEST(res);
  BOOST_TEST(order.action == Action::R);
  BOOST_TEST(order.product_id == "");
  BOOST_TEST(order.order_id == "100001");
  BOOST_TEST(order.side == Side::B);
  BOOST_TEST(order.quantity == 1);
  BOOST_TEST(order.price == 175);

  auto messages = { 
    "R,100001,B,1,175",
    "M,100001,B,1,175",
    "R,100001,S,1,175",
    "M,100001,S,1,175",
  };
  for(const string msg: messages) {
    vector<string> tokens;
    parse(msg, tokens);
    Order order;
    auto res = tokens_to_order(tokens, order);
    BOOST_TEST(res);
    BOOST_TEST(order.order_id == "100001");
    BOOST_TEST((order.action == Action::M || order.action == Action::R));
  }
}

BOOST_AUTO_TEST_CASE(test_tokens_to_order_for_trade_messages) {
  auto msg = "X,5,1,105";
  vector<string> tokens;
  parse(msg, tokens);
  Order order;
  auto res = tokens_to_order(tokens, order);
  BOOST_TEST(res);
  BOOST_TEST(order.action == Action::X);
  BOOST_TEST(order.product_id == "5");
  BOOST_TEST(order.order_id == "");
  BOOST_TEST(order.quantity == 1);
  BOOST_TEST(order.price == 105);

  auto messages = {
    "X,5,1,105",
    "X,153,21,2500"
  };
  for(const string msg: messages) {
    vector<string> tokens;
    parse(msg, tokens);
    Order message;
    auto res = tokens_to_order(tokens, message);
    BOOST_TEST(res);
    BOOST_TEST(message.action == Action::X);
  }
}

BOOST_AUTO_TEST_CASE(test_to_order_for_new_messages_1) {
  auto msg = "N,5,14211,B,3,1500";
  Order order;
  auto res = to_order(msg, order);
  BOOST_TEST(res);
  BOOST_TEST(order.action == Action::N);
  BOOST_TEST(order.product_id == "5");
  BOOST_TEST(order.order_id == "14211");
  BOOST_TEST(order.side == Side::B);
  BOOST_TEST(order.quantity == 3);
  BOOST_TEST(order.price == 1500);
}

BOOST_AUTO_TEST_CASE(test_to_order_for_new_messages_2) {
  auto msg = "N,5,14211,S,3,1500";
  Order order;
  auto res = to_order(msg, order);
  BOOST_TEST(res);
  BOOST_TEST(order.side == Side::S);

  auto messages = { 
    "N,5,14211,B,3,1500",
    "N,5,100001,B,1,75",
    "N,5,100001,S,17,1000",
  };
  for(const string msg: messages) {
    Order order;
    auto res = to_order(msg, order);
    BOOST_TEST(res);
    BOOST_TEST(order.action == Action::N);
    BOOST_TEST(order.product_id == "5");
  }
}

BOOST_AUTO_TEST_CASE(test_to_order_for_remove_modify_messages) {
  auto msg = "R,100001,B,1,175";
  Order order;
  auto res = to_order(msg, order);
  BOOST_TEST(res);
  BOOST_TEST(order.action == Action::R);
  BOOST_TEST(order.product_id == "");
  BOOST_TEST(order.order_id == "100001");
  BOOST_TEST(order.side == Side::B);
  BOOST_TEST(order.quantity == 1);
  BOOST_TEST(order.price == 175);

  auto messages = { 
    "R,100001,B,1,175",
    "M,100001,B,1,175",
    "R,100001,S,1,175",
    "M,100001,S,1,175",
  };
  for(const string msg: messages) {
    Order order;
    auto res = to_order(msg, order);
    BOOST_TEST(res);
    BOOST_TEST(order.order_id == "100001");
    BOOST_TEST((order.action == Action::M || order.action == Action::R));
  }
}

BOOST_AUTO_TEST_CASE(test_to_order_for_trade_messages) {
  auto msg = "X,5,1,105";
  Order order;
  auto res = to_order(msg, order);
  BOOST_TEST(res);
  BOOST_TEST(order.action == Action::X);
  BOOST_TEST(order.product_id == "5");
  BOOST_TEST(order.order_id == "");
  BOOST_TEST(order.quantity == 1);
  BOOST_TEST(order.price == 105);

  auto messages = {
    "X,5,1,105",
    "X,153,21,2500"
  };
  for(const string msg: messages) {
    Order order;
    auto res = to_order(msg, order);
    BOOST_TEST(res);
    BOOST_TEST(order.action == Action::X);
  }
}

BOOST_AUTO_TEST_SUITE_END()
