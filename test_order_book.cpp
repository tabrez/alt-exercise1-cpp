#define BOOST_TEST_MODULE "Test Order Book"
#include<boost/test/included/unit_test.hpp>
#include<algorithm>
#include"helpers.hpp"
#include"order_book.hpp"
using namespace std;

BOOST_AUTO_TEST_SUITE(TEST_ORDER_BOOK)

const Order& get_order_test(OrderBook& book, const string& msg) {
  Order order;
  to_order(msg, order);
  const auto& ret_order = book.get_order(order);
  BOOST_TEST(bool(ret_order));
  BOOST_TEST((*ret_order).order_id == order.order_id);
  return (*ret_order);
}

int compare_orders_and_get_quantity(OrderBook& book, const string& msg) {
  const auto& order = get_order_test(book, msg);
  return order.quantity;
}

BOOST_AUTO_TEST_CASE(test_get_order) {
  OrderBook book;
  auto msg = "N,5,100001,B,3,1500";
  book.insert_message(msg);
  Order order;
  to_order(msg, order);
  get_order_test(book, msg);
}

BOOST_AUTO_TEST_CASE(test_get_order_duplicate_id) {
  OrderBook book;
  auto msg = "N,5,100001,B,3,1500";
  book.insert_message(msg);

  msg = "N,4,100001,S,13,500";
  BOOST_TEST(!book.insert_message(msg));
  Order order;
  to_order(msg, order);
  const auto& ret_order = book.get_order(order);
  BOOST_TEST(bool(!ret_order));
}

BOOST_AUTO_TEST_CASE(test_insert_message) {
    auto msg = "N,5,14211,B,3,1500";
    OrderBook book;
    BOOST_TEST(book.insert_message(msg));
    BOOST_TEST(book.count() == 1);
    Order order;
    to_order(msg, order);
    get_order_test(book, msg);
}

// BOOST_AUTO_TEST_CASE(test_insert_order) {
//     auto msg = "N,5,14211,B,3,1500";
//     Order order;
//     to_order(msg, order);
//     OrderBook book;
//     BOOST_TEST(book.insert_order(order));
//     BOOST_TEST(book.count() == 1);
//     get_order_test(book, msg);
// }

BOOST_AUTO_TEST_CASE(test_insert_messages) {
  auto messages = {
    "N,5,100001,B,3,1500",
    "N,5,100002,B,1,175",
    "N,5,100003,B,1,175",
    "N,4,100004,B,1,200",
    "N,5,100005,B,1,175",
    "N,4,100006,S,1,175",
    "N,5,100007,S,1,200",
  };
  OrderBook book;
  auto i = 1;
  for(const string& msg: messages) {
    BOOST_TEST(book.insert_message(msg));
    BOOST_TEST((book.count() == i++));
    Order order;
    to_order(msg, order);
    get_order_test(book, msg);
  }
}

// BOOST_AUTO_TEST_CASE(test_insert_orders) {
//   auto messages = { 
//     "N,5,100001,B,3,1500",
//     "N,5,100002,B,1,175",
//     "N,5,100003,B,1,175",
//     "N,4,100004,B,1,200",
//     "N,5,100005,B,1,175",
//     "N,4,100006,S,1,175",
//     "N,5,100007,S,1,200",
//   };
//   OrderBook book;
//   auto i = 1;
//   for(const string& msg: messages) {
//     Order order;
//     to_order(msg, order);
//     BOOST_TEST(book.insert_order(order));
//     BOOST_TEST((book.count() == i++));
//     get_order_test(book, msg);
//   }
// }

BOOST_AUTO_TEST_CASE(test_insert_message_duplicate_order_id) {
  OrderBook book;
  book.insert_message("N,5,100001,B,3,1500");
  book.insert_message("N,5,100002,B,1,175");

  auto messages = { 
    "N,4,100002,S,3,1200",
    "N,5,100002,B,1,175",
    "N,5,100002,S,11,1200",
    "N,5,100001,S,11,1200"
  };
  
  BOOST_TEST((book.count() == 2));
  for(const string& msg: messages) {
    BOOST_TEST(!book.insert_message(msg));
    BOOST_TEST((book.count() == 2));
  }
}

BOOST_AUTO_TEST_CASE(test_count) {
  OrderBook book;
  auto msg = "N,5,100001,B,3,1500";
  book.insert_message(msg);
  BOOST_TEST(book.count() == 1);
}

BOOST_AUTO_TEST_CASE(test_repeat_insert) {
  const vector<string>& messages = {
    "N,3,100011,B,3,1500",
    "N,3,100012,B,3,500",
    "N,3,100013,S,4,1500",
    "N,3,100014,S,4,200",
    "N,3,100015,B,4,1500",
    "N,3,100016,S,3,500",

    "N,4,100017,B,3,1500",
    "N,4,100018,B,3,500",
    "N,4,100019,S,4,1500",
    "N,4,100020,S,4,200",
    "N,4,100021,B,4,1500",
    "N,4,100022,S,3,500",
  };
  OrderBook book;
  auto i = 1;
  for(const string& msg: messages) {
    BOOST_TEST(book.insert_message(msg));
    BOOST_TEST((book.count() == i++));
    Order order;
    to_order(msg, order);
    get_order_test(book, msg);
  }
}

BOOST_AUTO_TEST_CASE(test_repeat_insert_modify_remove) {
  vector<string> messages = {
    "N,5,100001,B,3,1500",
    "N,5,100002,B,4,175",
    "N,5,100003,S,2,175",
    "N,4,100004,B,11,200",
    "N,5,100005,B,7,175",
    "N,4,100006,S,8,175",
    "N,5,100007,S,12,200",
  };
  OrderBook book;
  book.process_vector(messages);
  auto msg = "N,5,100007,B,3,1200";   // duplicate
  int c = book.count();
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);
  msg = "N,4,100007,S,12,200";  // duplicate
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);

  msg = "M,100007,S,16,200";
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == c);
  BOOST_TEST((compare_orders_and_get_quantity(book, msg) == 16));

  msg = "R,100001,B,3,1500";
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == --c);
  msg = "R,100006,S,8,175";
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == --c);

  msg = "M,100002,B,3,175";
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == c);
  BOOST_TEST((compare_orders_and_get_quantity(book, msg) == 3));

  msg = "N,4,100008,B,3,1500";
  book.process_message(msg);
  c++;
  msg = "N,5,100001,B,13,500";
  BOOST_TEST(book.process_message(msg));
  c++;
  BOOST_TEST(book.count() == c);
  msg = "N,5,100001,B,3,1500";  // duplicate
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);

  msg = "R,100001,B,3,1500";    // doesn't exist
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);
  msg = "M,100001,B,17,500";
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == c);
  BOOST_TEST((compare_orders_and_get_quantity(book, msg) == 17));

  msg = "R,100001,B,17,500";    // does exist
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == --c);
  msg = "M,100001,B,17,500";    // doesn't exist
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);

  messages = {
    "M,100004,B,5,200",
    "N,4,100001,S,17,500",
    "N,5,100006,B,7,2500",
    "R,100005,B,7,175",
    "M,100001,S,8,500",
  };
  book.process_vector(messages);
  c += 1;
  BOOST_TEST(book.count() == c);

  msg = "M,100005,B,5,175";         // doesn't exist
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);
  msg = "M,100004,B,16,200";
  BOOST_TEST(book.process_message(msg));
  BOOST_TEST(book.count() == c);
  BOOST_TEST((compare_orders_and_get_quantity(book, msg) == 16));
  msg = "M,100006,S,16,2500";         // doesn't exist
  BOOST_TEST(!book.process_message(msg));
  BOOST_TEST(book.count() == c);
  msg = "M,100006,B,7,2500";
  BOOST_TEST((compare_orders_and_get_quantity(book, msg) == 7));
}

BOOST_AUTO_TEST_CASE(test_is_trade_message_expected_wrong_messages) {
  OrderBook book;
  const vector<string>& messages = {
    "X,5,2,1000",
    "M,4,100001,B,3,200",
    "M,4,100001,S,3,200",
    "N,2,100001,B,3,200",
  };
  for(const auto& msg: messages) {
    Order order;
    to_order(msg, order);
    BOOST_TEST(!book.is_trade_message_expected(order));
  }
  
  auto msg = "N,3,100001,B,3,1300";
  book.process_message(msg);
  const vector<string>& messages2 = {
    "X,3,3,1300",
    "M,3,100001,S,3,1300",
    "M,3,100001,S,3,1300",
    "M,3,100001,B,3,1300",
    "M,3,100001,B,3,1300",
    "N,3,100001,B,3,1300",
  };
  for(const auto& msg: messages2) {
    Order order;
    to_order(msg, order);
    BOOST_TEST(!book.is_trade_message_expected(order));
  }
}

BOOST_AUTO_TEST_CASE(test_is_trade_message_expected_when_not_expected) {
  const vector<string>& messages = {
    "N,3,100011,B,3,1300",
    "N,3,100012,B,3,500",
    "N,3,100013,S,4,1500",
    "N,3,100014,S,4,2100",
    "N,3,100015,B,4,1400",
    "N,3,100016,S,3,1800",

    "N,4,100019,S,4,2000",
    "N,4,100020,S,4,2200",
    "N,4,100017,B,3,1500",
    "N,4,100018,B,3,500",
    "N,4,100021,B,4,500",
    "N,4,100022,S,3,1600",

    // not enough quantity
    "N,5,100023,B,13,1300",
    "N,5,100024,S,14,1290",
    "N,5,100025,B,15,1295",
    "N,5,100026,S,29,1290",
  };
  OrderBook book;
  for(const auto& msg: messages) {
    Order order;
    to_order(msg, order);
    BOOST_TEST(!book.is_trade_message_expected(order));
    book.process_message(msg);
  }
}

BOOST_AUTO_TEST_CASE(test_is_trade_message_expected_when_wrong_product_id) {
  OrderBook book;
  book.process_message("N,3,100011,B,10,1300");
  const vector<string>& messages = {
    "N,4,100012,S,9,1290",
    "N,3,100013,B,8,1295",
    "N,4,100014,B,25,1150",
    "N,3,100015,S,19,1100",
    "N,3,100016,S,19,1100",
    "N,4,100017,B,19,1100",
  };
  for(const auto& msg: messages) {
    Order order;
    to_order(msg, order);
    BOOST_TEST(!book.is_trade_message_expected(order));
    book.process_message(msg);
  }
}

BOOST_AUTO_TEST_CASE(test_is_trade_message_expected_when_expected) {
  OrderBook book;
  book.process_message("N,3,100011,B,16,1300");
  book.process_message("N,4,100019,S,4,2000");
  book.process_message("N,4,100020,S,4,2200");

  const vector<string>& messages = {
    "N,3,100012,S,14,1290",
    "N,3,100014,B,9,1295",
    "N,3,100013,S,7,1100",
    "N,3,100015,B,5,1150",

    "N,4,100017,B,7,2500",
    "N,4,100018,B,8,2300",
    "N,4,100021,B,4,2100",
    "N,4,100022,S,11,2000",
  };
  for(const auto& msg: messages) {
    Order order;
    to_order(msg, order);
    BOOST_TEST(book.is_trade_message_expected(order));
    book.process_message(msg);
  }
}

BOOST_AUTO_TEST_SUITE_END()
