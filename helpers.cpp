#include<iostream>
#include<regex>
#include<utility>
#include <boost/algorithm/string.hpp>
#include "helpers.hpp"

using namespace std;

const int Limits::max_price = 10000;

string to_string(const Action& action) {
  if (action == Action::N)
    return "N";
  else if (action == Action::M)
    return "M";
  else if (action == Action::R)
    return "R";
  // else if (action == Action::X)
  return "X";
}

ostream& operator << (ostream& os, const Action& action) {
  return os << to_string(action);
}

string to_string(const Side& side) {
  return side == Side::B ? "B" : "S";
}

ostream& operator << (ostream& os, const Side& side) {
  return os << to_string(side);
}

string to_string(const Order& order) {
  string res;
  res += "{ " + to_string(order.action) + ", " ;
  if (order.product_id != "")
    res += order.product_id + ", ";
  if (order.order_id != "")
    res += order.order_id  + ", ";
  if (order.action != Action::X)
    res += to_string(order.side) + ", ";
  res += to_string(order.quantity) + ", " + to_string(order.price) + " }";
  return res;
}

ostream& operator << (ostream& os, const Order& order) {
  return os << to_string(order);
}

bool operator == (const struct Order& first, const struct Order& second) {
  return (first.action == second.action &&
          first.product_id == second.product_id &&
          first.order_id == second.order_id &&
          first.side == second.side &&
          first.quantity == second.quantity &&
          first.price == second.price);
}

bool validate_message(const string& message) {
  // might want to ignore leading zeroes in quantity & price fields
  const auto pattern = regex("^(N,\\d{1,5}|R|M),\\d{1,10},[S|B],\\d{1,3},\\d{1,6}$|^X,\\d{1,5},\\d{1,3},\\d{1,6}$$");
  return regex_search(message, pattern);
}

void parse(const string& msg, vector<string>& tokens) {
  boost::split(tokens, msg, boost::is_any_of(","));
  transform(tokens.cbegin(), tokens.cend(), tokens.begin(), [](string token){
    boost::trim(token); return token;
  });
}

Action to_action_value(const string token) {
  if (token == "N")
    return Action::N;
  else if (token == "M")
    return Action::M;
  else if (token == "R")
    return Action::R;
  else // (token == "X")
    return Action::X;
}

Side to_side_value(const string token) {
  if (token == "B")
    return Side::B;
  else // token == "S"
    return Side::S;
}

bool tokens_to_order(const vector<string>& tokens, Order& order) {
  const int8_t len = tokens.size();
  order.quantity = stoi(tokens[len - 2]);
  order.price = stoi(tokens[len - 1]);
  // can integrate checks against Limits::max_price etc.
  if (order.quantity == 0 || order.price == 0) {
    return false;
  }

  order.action = to_action_value(tokens[0]);
  if (tokens[0] == "N") {
    order.product_id = tokens[1];
    order.order_id = tokens[2];
    order.side = to_side_value(tokens[3]);
  } else if (tokens[0] == "M" || tokens[0] == "R") {
    order.order_id = tokens[1];
    order.side = to_side_value(tokens[2]);
  } else { // if(tokens[0] == "X")
    order.product_id = tokens[1];
  }
  if(order.price > Limits::max_price) return false;
  return true;
}

// re-implement as member method of OrderBook to avoid pointer argument
bool to_order(const string& msg, Order& order, vector<string> *errors) {
  if (!validate_message(msg)) {
    if(errors != NULL) {
      errors->push_back("Corrupted message format: " + msg);
    }
    return false;
  }
  vector<string> tokens;
  parse(msg, tokens);
  return tokens_to_order(tokens, order);
}
