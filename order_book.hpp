#ifndef ORDER_BOOK_HPP
#define ORDER_BOOK_HPP
#include<unordered_map>
#include<map>
#include<vector>
#include<iostream>
#include<boost/optional.hpp>
#include"helpers.hpp"
using namespace std;

struct RecentTrade {
  int quantity;
  int price;
};

using OrdersType = unordered_map<string, map<int, map<string, Order>>>;
using PriceTreeType = std::map<int, std::map<std::string, Order>>;
using MatchingOrdersType = std::tuple<OrdersType &, 
                                      std::map<int, std::map<std::string, Order>> &,
                                      std::map<std::string, Order> &,
                                      const std::string &>;

class OrderBook {
  private:
    OrdersType buy_orders;
    OrdersType sell_orders;
    unordered_map<string, string> po_map;
    unordered_map<string, RecentTrade> recent_trades;
    bool trade_msg_expected;
    vector<string> errors;

  private:
    boost::optional<MatchingOrdersType> get_matching_orders(const Order& order);
    bool insert_order(const Order& order);
    bool modify_order(const Order& mod_order);
    bool remove_order(const Order& rem_order);
    pair<bool, bool> is_it_expected_message(const string& msg, const Order& order);
    void update_recent_trades(const Order& order);
    void process_msg_and_output(const string& msg, int8_t count);
    void products_to_string(const OrdersType& all_orders, string& output) const;
    int count_orders(const OrdersType& all_orders) const;

  public:
    OrderBook();

    bool insert_message(const string& msg);
    bool modify_message(const string& msg);
    bool remove_message(const string& msg);

    bool process_message(const string& msg);
    bool process_file(const string& filename);

    void print_products(string& output) const;
    void print_errors(string& output) const;

    // used internally or for testing and debugging purposes
    int count() const;
    boost::optional<const Order&> get_order(const Order& order);
    bool is_trade_message_expected(const Order& order);
    void process_vector(const vector<string>& messages);
    void print_orders(const OrdersType& orders) const;
    void print_report() const;
};
#endif
