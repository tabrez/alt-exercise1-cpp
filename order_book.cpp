#include<iostream>
#include<fstream>
#include<vector>
#include<unordered_map>
#include<map>
#include<boost/optional.hpp>
#include"order_book.hpp"
#include"helpers.hpp"
using namespace std;

OrderBook::OrderBook() {
  trade_msg_expected = false;
}

bool insert_new_product_entry(OrdersType& orders, const Order& order) {
  map<string, Order> orders_map = {{ order.order_id, order }};
  map<int, map<string, Order>> m = {{ order.price, orders_map }};
  orders.insert({ order.product_id, m });
  return true;
}

bool insert_or_assign(PriceTreeType& tree, const Order& order) {
  map<string, Order> entry = {{ order.order_id, order }};
  if (tree.count(order.price) == 0) {
    tree.insert({order.price, entry});
    return true;
  }
  tree[order.price][order.order_id] = order;
  return false;
}

bool OrderBook::insert_order(const Order& order) {
  if (po_map.count(order.order_id) != 0 )
    return false;
  po_map[order.order_id] = order.product_id;

  auto& all_orders = order.side == Side::B ? buy_orders : sell_orders;

  if (all_orders.count(order.product_id) == 0)
    return insert_new_product_entry(all_orders, order);

  auto& tree = all_orders[order.product_id];
  // or can use `map::insert_or_assign` from c++17
  insert_or_assign(tree, order);
  return true;
}

bool OrderBook::insert_message(const string& msg) {
  Order order;
  auto res = to_order(msg, order);
  if(!res) return false;
  return insert_order(order);
}

boost::optional<MatchingOrdersType> OrderBook::get_matching_orders(const Order& order) {
  if (po_map.count(order.order_id) == 0)
    return boost::none;
  const auto& product_id = po_map.at(order.order_id);
  auto& all_orders = order.side == Side::B ? buy_orders : sell_orders;

  auto tree_iter = all_orders.find(product_id);
  if(tree_iter == all_orders.end())
    return boost::none;

  auto& tree = tree_iter->second;
  auto orders_iter = tree.find(order.price);
  if(orders_iter == tree.end())
    return boost::none;

  return forward_as_tuple(all_orders, tree, orders_iter->second, product_id);
}

bool OrderBook::modify_order(const Order& mod_order) {
  auto mo = get_matching_orders(mod_order);
  if(!mo) return false;

  auto& orders = get<2>(*mo);
  auto orders_iter = orders.find(mod_order.order_id);
  if(orders_iter == orders.end())
    return false;

  auto& order = orders_iter->second;
  order.quantity = mod_order.quantity;
  return true;
}

bool OrderBook::modify_message(const string& msg) {
  Order order;
  if(!to_order(msg, order))
    return false;
  return modify_order(order);
}

void cleanup_after_remove(MatchingOrdersType& m, const Order& order) {
  auto& all_orders = get<0>(m);
  auto& tree = get<1>(m);
  auto& product_id = get<3>(m);

  // if removing the order with order.order_id makes the tree for this price
  // empty, remove this price tree also
  if(tree.at(order.price).size() == 0)
    tree.erase(order.price);
  // if removing the above  tree makes the hash table slot for this product
  // empty, remove the slot also
  if(all_orders.at(product_id).size() == 0)
    all_orders.erase(product_id);
}

bool OrderBook::remove_order(const Order& rem_order) {
  auto res = get_matching_orders(rem_order);
  if(!res)
    return false;
  // c++17: auto&[all_orders, tree, product_id] = *res
  // Or: std::tie(all_orders, tree, product_id) = *res
  auto& orders = get<2>(*res);

  auto orders_iter = orders.find(rem_order.order_id);
  if(orders_iter == orders.end())
    return false;
  auto& order = orders_iter->second;

  if(order.quantity != rem_order.quantity)
    return false;

  orders.erase(order.order_id);
  cleanup_after_remove(*res, rem_order);
  po_map.erase(order.order_id);
  return true;
}

bool OrderBook::remove_message(const string& msg) {
  Order order;
  if(!to_order(msg, order))
    return false;
  return remove_order(order);
}

bool is_quantity_available(PriceTreeType::const_iterator lower_bound,
                              PriceTreeType::const_iterator upper_bound,
                              const int order_quantity) {
  auto quantity = 0;
  for(auto iter = lower_bound; iter != upper_bound; ++iter) {
    const auto& orders = iter->second;
    for(auto iter2 = orders.begin(); iter2 != orders.end(); ++iter2) {
      quantity += iter2->second.quantity;
      if(quantity >= order_quantity)
        return true;
    }
    // return true here if trades for partial quantities are allowed and quantity > 0
  }

  return false;
}

bool OrderBook::is_trade_message_expected(const Order& order) {
  // If 'M,x,x,x,x' messages can also  trigger a trade, include `Action::M` in
  // the following condition and fetch product id from `po_map`(see python version)
  if (order.action != Action::N)
    return false;
  if(po_map.find(order.order_id) != po_map.end())
    return false;

  const auto& all_orders = order.side == Side::B ? sell_orders : buy_orders;
  const auto products_iter = all_orders.find(order.product_id);
  if(products_iter == all_orders.end())
    return false;
  const auto& products = products_iter->second;

  PriceTreeType::const_iterator lower_bound =
      order.side == Side::B ?
        products.lower_bound(0) :
        products.lower_bound(order.price);

  PriceTreeType::const_iterator upper_bound =
      order.side == Side::B ?
        products.upper_bound(order.price) :
        products.upper_bound(Limits::max_price);

  // we can store cumulative quantity for each price bracket at the root of
  // price tree to make this task faster at the cost of extra memory
  return is_quantity_available(lower_bound, upper_bound, order.quantity);
}

pair<bool, bool> OrderBook::is_it_expected_message(const string& msg, const Order& order) {
  //  `status` variable is helpful for unit testing & debugging:
  //  status.first = true means a trade message was expected but not received
  //  status.second = true means a trade was not expected but was received
  auto status = make_pair(false, false);

  if(!trade_msg_expected && order.action == Action::X) {
    status.second = true;
    errors.push_back("Recieved a trade action when such trade is not possible;" \
                        " receieved message: " + msg);
    return status;
  }
  if(trade_msg_expected && order.action != Action::X) {
    status.first = true;
    errors.push_back("Trade is expected but no trade action received; "\
                        "received message: " + msg);
  }
  return status;
}

void OrderBook::update_recent_trades(const Order& order) {
  auto recent_trades_iter = recent_trades.find(order.product_id);
  if(recent_trades_iter == recent_trades.end()) {
    recent_trades[order.product_id] =  {
      order.quantity, order.price
    };
    return;
  }

  auto& product = recent_trades_iter->second;
  if(product.price == order.price)
    product.quantity = product.quantity + order.quantity;
  else {
    product.price = order.price;
    product.quantity = order.quantity;
  }
}

boost::optional<const Order&> OrderBook::get_order(const Order& ret_order) {
  auto res = get_matching_orders(ret_order);
  if(!res)
    return boost::none;

  auto& price_tree = get<1>(*res);
  auto& orders = get<2>(*res);
  auto order_iter = orders.find(ret_order.order_id);
  if(order_iter == orders.end() ||
      order_iter->second.order_id != ret_order.order_id)
        return boost::none;

  return order_iter->second;
}

void OrderBook::products_to_string(const OrdersType& all_orders, string& output) const {
  for(const auto& product: all_orders) {
    output += "\nProduct ID: " + product.first + " =>";
    // use iterators if number of records printed should be limited
    for(const auto& orders: product.second) {
      output += "\n--Price: " + to_string(orders.first);
      for(const auto& order_pair: orders.second) {
        const auto& order = order_pair.second;
        output += "\n    order-id: " + order.order_id;
        output += "\n    side: " + to_string(order.side);
        output += "\n    quantity: " + to_string(order.quantity);
        output += "\n";
      }
    }
  }
}

void OrderBook::print_products(string& output) const {
  output += "\nBEGIN++++++++++++++++++++++++++++++++++++";
  output += "\n---Buy Orders---";
  products_to_string(buy_orders, output);
  output += "\n---Sell orders---";
  products_to_string(sell_orders, output);
  output += "\nEND==================================";
}

void OrderBook::print_errors(string& output) const {
  output += "\n\nBEGIN++++++++++++++++++++++++++++++++++++";
  output += "\n---Errors---";
  for(auto& err: errors)
    output += '\n' + err;
  output += "\nEND==================================";
}

int OrderBook::count_orders(const OrdersType& all_orders) const {
  auto count = 0;
  for(const auto& trees: all_orders)
    for(const auto& orders: trees.second)
      count += orders.second.size();
  return count;
}

int OrderBook::count() const {
  return count_orders(buy_orders) + count_orders(sell_orders);
}

void OrderBook::print_orders(const OrdersType& orders) const {
  for(const auto& trees: orders) {
    for(const auto& orders: trees.second) {
      for(const auto& order: orders.second) {
        cout << endl << order.second;
      }
    }
  }
}

void OrderBook::print_report() const {
  cout << endl << endl << "BEGIN++++++++++++++++++++++++++++++++++++";
  cout << endl << "order book: ";
  print_orders(buy_orders);
  print_orders(sell_orders);
  cout << endl << "END==================================";
}

bool OrderBook::process_message(const string& msg) {
  Order order;
  if(!to_order(msg, order, &errors))
    return false;

  auto handle_new_order = [this, &msg](const Order& order) {
    auto res = insert_order(order);
    if(!res)
      errors.push_back("Order with id " + order.order_id + " already exists;" \
                        " received message: " + msg);
    return res;
  };

  auto handle_modify_order = [this, &msg](const Order& order) {
    auto res = modify_order(order);
    if(!res)
      errors.push_back("Order with specified details doesn't exist;" \
                        " received message: " + msg);
    return res;
  };

  auto handle_remove_order = [this, &msg](const Order& order) {
    auto res = remove_order(order);
    if(!res)
      errors.push_back("Order with specified details doesn't exist;" \
                        " received message: " + msg);
    return res;
  };

  auto handle_trade_order = [this, &msg](const Order& order) {
    update_recent_trades(order);
    const auto& trade = recent_trades[order.product_id];
    cout << endl << msg << " => product " + order.product_id << ": ";
    cout << to_string(trade.quantity) << " @ " << to_string(trade.price);
    return true;
  };

  is_it_expected_message(msg, order);
  trade_msg_expected = is_trade_message_expected(order);

  switch(order.action) {
    case Action::N:
      return handle_new_order(order);
      break;
    case Action::M:
      return handle_modify_order(order);
      break;
    case Action::R:
      return handle_remove_order(order);
      break;
    // case Action::X:
    default:
      return handle_trade_order(order);
      break;
  }
}

void OrderBook::process_msg_and_output(const string& msg, int8_t count) {
  auto success = process_message(msg);
  if(!success)
    return;
  if (count == 9) {
    cout << endl << endl << "BEGIN++++++++++++++++++++++++++++++++++++";
    cout << endl << count + 1 << " more messages processed, order book as follows: ";
    string products_str;
    print_products(products_str);
    cout << products_str;
    cout << endl << "END==================================";
  }
}

void OrderBook::process_vector(const vector<string>& messages) {
  int8_t count = 0;
  for (const string& msg: messages) {
    process_msg_and_output(msg, count);
    count = (count + 1) % 10;
  }
}

bool OrderBook::process_file(const string& filename) {
  ifstream file(filename);
  // c++17: std::filesystem::exists(filename);
  // boost: boost::filesystem::exists(filename)
  if(!file.good()) {
    cout << endl << "No file named `messages.txt` exists; aborting.";
    return false;
  }

  string msg;
  int8_t count = 0;
  while(getline(file, msg)) {
    process_msg_and_output(msg, count);
    count = (count + 1) % 10;
  }
  return true;
}
