# C++ Version

## Running the program

I've developed the program using g++ 9.3.0 on Ubuntu system that has all the
boost 1.71 libraries installed. I'm using the following boost libraries:

* Boost.Optional
* Boost.Algorithms.String
* Boost.Test (only needed if running unit tests)

I've installed the boost libraries using the following command:

```bash
sudo apt install libboost-all-dev
```

To run the program:

```bash
g++ -std=c++11 examples.cpp order_book.cpp helpers.cpp -o exercise && ./exercise
```

To run the unit tests:

```bash
# test order book
g++ -std=c++11 order_book.cpp test_order_book.cpp helpers.cpp -o tests && ./tests
# test helpers
g++ -std=c++11 order_book.cpp test_helpers.cpp helpers.cpp -o tests && ./tests
```

The program will read messages stored in a file called `messages.txt` in the
current directory and process them.

I've used `clang-tidy` as the linting tool integrated with VS Code using `notskm.clang-tidy` extension.

```bash
sudo apt install clang-tidy
code --install-extension notskm.clang-tidy
```

## File Structure

* `order_book.cpp` has the main implementation of the order book. `order_book.hpp`
is its header file.
* `examples.cpp` is the driver program and has couple of examples in it, including
one that processes messages from an array instead of from a file.
* `helpers.cpp` has helper/util functions. `helpers.hpp` is its header file.

* `test_order_book.cpp` have unit tests for `order_book.cpp`.
* `test_helpers.cpp` has unit tests for `helpers.cpp`.

* `messages.txt` has example messages. This can be replaced by your own file.
* `corrupted_messages.txt` has examples of different types of corrupted messages.

Also see `messages_explanation` and `assumptions.md` from the Python
repository.
