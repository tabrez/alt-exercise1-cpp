#ifndef HELPERS_HPP
#define HELPERS_HPP
#include<string>
#include<vector>
using namespace std;

enum Action { N, M, R, X };

enum Side { B, S };

struct Order {
  Action action;
  string product_id;
  string order_id;
  Side side;
  int price;
  int quantity;
};


// add more limits here as needed for quantity, number of digits in
// product_id/order_id, then use them in validation function
struct Limits {
  static const int max_price;
};

class OrderBook;

string to_string(const Action& action);
string to_string(const Side& side);
ostream& operator << (ostream& os, const Order& order);
bool operator == (const struct Order& first, const struct Order& second);
bool to_order(const string& msg, Order& order, vector<string> *errors = NULL);

#endif
